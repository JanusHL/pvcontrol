# pvcontrol
I've worked for years with Omron PLC's and I programmed a simple function to read a power meter for my FV system. In 2018 I moved to Raspberry pi to take more control of the system. In this repository I want to leave the utilities I use to do the job.

# Hardware
2 x Victron Multiplus 2000/24

2 x Midnite Classic 150

BMV700

Raspberry PI + PVControl: Core of data control

ESP2866: To get some external data and send it to RPI via MQTT.

Actually testing some energy meters with modbus interface

# Tools for raspberry pi.
Based in the PVControl development by MLeon and Nikkito.

Some other code utilities to manage data...

You can find me at https://adnsolar.eu
